// This is the View
// Its only job is to display what the user sees
// It performs no calculations, but instead passes
// information entered by the user to whomever needs
// it. 


import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class CalculatorView extends JFrame implements ActionListener{

	private JTextField displayNumber  = new JTextField(10);
	private JButton additionButton = new JButton("+");
	private JButton multiplicationButton = new JButton("*");
	private JButton calculationButton = new JButton("=");
	private JButton oneButton = new JButton("1");
	private JButton twoButton = new JButton("2");
	private JButton threeButton = new JButton("3");
	private JButton fourButton = new JButton("4");
	private JButton fiveButton = new JButton("5");
	private JButton sixButton = new JButton("6");
	private JButton sevenButton = new JButton("7");
	private JButton eightButton = new JButton("8");
	private JButton nineButton = new JButton("9");
	private JButton zeroButton = new JButton("0");
	
	private int bwidth = 40;
	private int bheight = 40;
	private int previousDisplayNumber = 0;
	
	CalculatorView(){
		
		// Sets up the view and adds the components
		
		JPanel calcPanel = new JPanel();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(400, 500);

		//Setting setLayout to 'null' means
		//that you have to use .setBounds to define location and size for each item.
		calcPanel.setLayout(null);
		
		
		//Set bounds of each item button
		additionButton.setBounds ( 160,100,bwidth, bheight);
		multiplicationButton.setBounds (160,150,bwidth, bheight);
		calculationButton.setBounds (160,200,bwidth,bheight);
		
		
		//Number Buttons
		oneButton.setBounds(10,100,bwidth,bheight);
		
		twoButton.setBounds(60,100,bwidth,bheight);
		
		threeButton.setBounds(110,100,bwidth,bheight);
		fourButton.setBounds(10,150,bwidth,bheight);
		fiveButton.setBounds(60,150,bwidth,bheight);
		sixButton.setBounds(110,150,bwidth,bheight);
		sevenButton.setBounds(10,200,bwidth,bheight);
		eightButton.setBounds(60,200,bwidth,bheight);
		nineButton.setBounds(110,200,bwidth,bheight);
		
		
		//Set bounds of each text item
		displayNumber.setBounds(100,30,100,30);
		
		
		
		//this.additionButton.setSize(50, 50);
		
		
		
		calcPanel.add(displayNumber);
		calcPanel.add(multiplicationButton);
		calcPanel.add(additionButton);
		calcPanel.add(calculationButton);
		
		calcPanel.add(oneButton);
		calcPanel.add(twoButton);
		calcPanel.add(threeButton);
		calcPanel.add(fourButton);
		calcPanel.add(fiveButton);
		calcPanel.add(sixButton);
		calcPanel.add(sevenButton);
		calcPanel.add(eightButton);
		calcPanel.add(nineButton);
		calcPanel.add(zeroButton);
		
		
		
		this.add(calcPanel);
		
		// End of setting up the components --------
		
	}
	
	

	//=====
	//  Button Listeners
	//=====

	void oneListener(ActionListener listenForOneButton){
	
		oneButton.addActionListener(listenForOneButton);
	}
	
	void addAdditionListener(ActionListener listenForAddButton){
		
		additionButton.addActionListener(listenForAddButton);
		
	}
	
	void addMultiplicationListener(ActionListener listenForMultiplicationButton){
		
		multiplicationButton.addActionListener(listenForMultiplicationButton);
		
	}
	
	
	void addCalculationListener(ActionListener listenForCalculationButton){
		
		calculationButton.addActionListener(listenForCalculationButton);
		
	}

	//=====
	//  Display Listeners
	//=====


	public int getPreviousDisplayNumber(){
		return previousDisplayNumber;
	}
	
	public void setPreviousDisplayNumber(int pdn){
		previousDisplayNumber = pdn;
	}
	
	public int getDisplayNumber(){
		
		return Integer.parseInt(displayNumber.getText());
		
	}
	
	public void setDisplayNumber(int solution){
		
		displayNumber.setText(Integer.toString(solution));
		
	}
	
	// If the calculateButton is clicked execute a method
	// in the Controller named actionPerformed
	

	

	
	// Open a popup that contains the error message passed
	
	void displayErrorMessage(String errorMessage){
		
		JOptionPane.showMessageDialog(this, errorMessage);
		
	}
	
}