import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// The Controller coordinates interactions
// between the View and Model

public class CalculatorController {
	
	private CalculatorView theView;
	private CalculatorModel theModel;
	
	public CalculatorController(CalculatorView theView, CalculatorModel theModel) {
		this.theView = theView;
		this.theModel = theModel;
		
		// Tell the View that when ever the calculate button
		// is clicked to execute the actionPerformed method
		// in the CalculateListener inner class
		
		
		this.theView.addMultiplicationListener(new MultiplyListener());
		this.theView.addAdditionListener(new AdditionListener());
		this.theView.oneButtonListener(new oneListener());
		//this.theView.twoButtonListener(new twoListener());
		//this.theView.threeButtonListener(new threeListener());
	}
	
	class oneListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			int previousNumber, displayNumber = 0;
			
			try{
			
			
				previousNumber = theView.getPreviousDisplayNumber();
				displayNumber = previousNumber+1;
				
				theView.setDisplayNumber(displayNumber);
			} 
			
			catch(NumberFormatException ex){
				
				System.out.println(ex);
				
				theView.displayErrorMessage("You Need to Enter Integers");
				
			}
			
		}
	}
	
	
	class twoListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			int previousNumber, displayNumber = 0;
			
			try{
				
			/*	theView.displayErrorMessage(e.getSource().toString());
				theView.displayErrorMessage(e.getActionCommand().toString());
				theView.displayErrorMessage(e.getClass().toString());
				theView.displayErrorMessage(Integer.toString(e.getID())); */
				
				theView.setDisplayNumber(2);
				
				
				previousNumber = theView.getPreviousDisplayNumber();
				displayNumber = theView.getDisplayNumber();
			} 
			
			catch(NumberFormatException ex){
				
				System.out.println(ex);
				
				theView.displayErrorMessage("You Need to Enter Integers");
				
			}
			
		}
	}
	
	class AdditionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			
			int firstNumber, secondNumber = 0;
			
			// Surround interactions with the view with
			// a try block in case numbers weren't
			// properly entered
			
			try{
			
				firstNumber = theView.getDisplayNumber();
				secondNumber = theView.getPreviousDisplayNumber();
				
				
				theModel.addTwoNumbers(firstNumber, secondNumber);
				
				theView.setDisplayNumber(theModel.getCalculationValue());
				theView.setPreviousDisplayNumber(theModel.getCalculationValue());
				
			
			}

			catch(NumberFormatException ex){
				
				System.out.println(ex);
				
				theView.displayErrorMessage("You Need to Enter 2 Integers");
				
			}
			
		}
	}
		class MultiplyListener implements ActionListener{

			public void actionPerformed(ActionEvent e) {
				
				int firstNumber, secondNumber = 0;
				
				// Surround interactions with the view with
				// a try block in case numbers weren't
				// properly entered
				
				try{
				
					firstNumber = theView.getDisplayNumber();
					secondNumber = theView.getPreviousDisplayNumber();
					
					theModel.multiplyTwoNumbers(firstNumber, secondNumber);
					
					
					
					theView.setDisplayNumber(theModel.getCalculationValue());
					theView.setPreviousDisplayNumber(theModel.getCalculationValue());
				
				}

				catch(NumberFormatException ex){
					
					System.out.println(ex);
					
					theView.displayErrorMessage("You Need to Enter 2 Integers");
					
				}
				
			}
		
	}
}
	
